import { MutationTree, ActionTree } from 'vuex'
// import { set, toggle } from '@/utils/vuex'

export const strict = false

export const state = () => ({
  version: process.env.version,
  debug: process.env.NODE_ENV === 'development',

  // dark: false,
})
export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  // setDark: set('dark'),
  // toggleDark: toggle('dark'),
}

export const actions: ActionTree<RootState, RootState> = {}
