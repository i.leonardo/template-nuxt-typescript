import objectPath from 'object-path'

// Check key exist Object
export const has = (obj: Object, property: string | string[]) => {
  return objectPath.has(obj, property)
}

// Promise setTimeout
export const wait = (ms: number) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}
