import objectPath from 'object-path'

export const set = (property: string | string[]) => (
  store: object,
  payload: any
) => {
  objectPath.set(store, property, payload)
}

export const objAssign = (property: string | string[]) => (
  store: object,
  payload: any
) => {
  const val = Object.assign({}, objectPath.get(store, property), payload)
  objectPath.set(store, property, val)
}

export const toggle = (property: string | string[]) => (store: object) => {
  objectPath.set(store, property, !objectPath.get(store, property))
}
