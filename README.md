# Nuxt + TypeScript + Eslint + Prettier + SCSS

> Template Possui configurações para o Vuetify 2, exemplos do uso Env e alguns códigos úteis para trabalhar com Vuex.

Um modelo de projeto inicial do [Nuxt.js](https://github.com/nuxt/nuxt.js) com um ambiente personalizado para o desenvolvimento com Typescript e com ferramenta de Lint para verificar seu código e Prettier para organizar.

![Main](https://gitlab.com/i.leonardo/template-nuxt-typescript/raw/master/main.png)

## Instalação

Este template é gerado através do Legacy API [@vue/cli-init](https://cli.vuejs.org/guide/creating-a-project.html#using-the-gui):

``` bash
# remote template
$ vue init gitlab:i.leonardo/template-nuxt-typescript my-project

# install dependencies
$ cd my-project
$ npm install
```

## Uso

### Development

``` bash
# build for development and launch the server
$ npm run dev
```

Acessa o site: [http://localhost:3000](http://localhost:3000)

### Production

``` bash
# build for production and launch the server
$ npm run build
$ npm start
```

Acessa o site: [http://localhost:3000](http://localhost:3000)

### Static Generation

``` bash
$ npm run generate
```

Estrutura do site vai está em `/dist`

## Licença

> [MIT License](https://gitlab.com/i.leonardo/template-nuxt-typescript/blob/master/LICENSE)

Copyright © 2020 [Leonardo C. Carvalho](https://gitlab.com/i.leonardo)
